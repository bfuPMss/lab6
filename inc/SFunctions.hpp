#pragma once

#include <iostream>
#include "Functions.cpp"



using namespace ct;

{

	void Read(int& n, int matrix[N][N]);

	bool IsDiagMax(int n, int i, int matrix[N][N]);

	bool IsAllDiagMax(int n, int matrix[N][N]);

	int Mult(int n, int matrix[N][N]);

	bool ZeroCheck(int x);

	void Replacement(int n, int matrix[N][N]);

	void Write(int n, int matrix[N][N]);

}